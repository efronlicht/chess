# build on rust-latest
FROM rustlang/rust:nightly as builder
WORKDIR /usr/src/chess-server
COPY . .
RUN cargo +nightly install --path ./server

# run on a slimmed down debian
# we could compile for x64_unknown_linux_musl and use alpine to save even more space, but this is fine for now.
FROM debian:buster-slim
RUN apt-get update && apt-get dist-upgrade -y && apt-get upgrade -y && apt-get autoremove
COPY --from=builder /usr/local/cargo/bin/chess-server /usr/local/bin/chess-server
EXPOSE 8000
CMD ["chess-server"]