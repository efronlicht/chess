# chess

## organization

- ### [game](./game) contains a standalone implementation of the game logic.

- ### [server](./server) contains a rocket server.

## installation

## local

### dependencies

- rust (nightly) [install via rustup](https://rustup.rs/), then `rustup install nightly`

```sh
git clone https://gitlab.com/efronlicht/chess
cd chess/server
```

then one of

    cargo +nightly run # debug build
    cargo +nightly test # tests
    cargo install --path . # install

## via dockerfile

### dependencies

- [install docker](https://www.docker.com/get-started)

```sh
git clone https://gitlab.com/efronlicht/chess
cd chess
docker build -t chess:latest
docker run -p <PORT>:8000 chess:latest  # run the server, exposing it at <PORT>. eg, use 8000:8000 if you want localhost:8000 to be the given port.
```

shutdown:
