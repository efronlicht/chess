//! chess runs a game of chess. part of the project (https://gitlab.com/efronlicht/chess)
#![feature(proc_macro_hygiene, decl_macro)]
extern crate serde;
pub mod text;

use serde::{Deserialize, Serialize};
use Color::{Black, White};
use Kind::*;

type Square = Option<Piece>;
#[derive(Copy, Clone, PartialEq, Eq, Debug, Serialize, Deserialize)]
/// a traditional 8x8 chessboard. this is zero-indexed and starts with the black side.
/// that is, c7 in algebraic notation corresponds to (1, 2).
/// see [from_algebraic] for details on conversion

pub struct Board(pub [[Square; 8]; 8]);
#[derive(Copy, Clone, PartialEq, Debug, Eq, Serialize, Deserialize)]
/// a representation of the current state of a chess game.
/// chess is almost perfectly stateless, except for en-passant and castling.
pub struct Gamestate {
    pub board: Board,
    pub turn: usize,
    en_passant: Enpassant,
    castling: CastlingAllowed,
}
/// convert positional notation to a positional index for the [Board]. inverse of [to_positional].
pub fn try_from_positional(s: &str) -> Option<(i8, i8)> {
    if s.len() != 2 {
        None
    } else {
        let mut bytes = s.bytes();
        match (bytes.next().zip(bytes.next()))? {
            (col @ b'a'..=b'h', row @ b'1'..=b'8') => {
                Some(((row - b'1') as i8, (col - b'a') as i8))
            }
            (col @ b'A'..=b'H', row @ b'1'..=b'8') => {
                Some(((row - b'1') as i8, (col - b'A') as i8))
            }
            _ => None,
        }
    }
}
/// convert the [Board]'s positional index to classic positional (i.e, "e2", "f6"). inverse of [try_from_algebraic].
/// this will panic if the index is out of bounds in debug builds.
pub fn to_positional((row, col): (i8, i8)) -> (char, char) {
    debug_assert!(row < 8 && row >= 0 && col < 8 && col > 0);
    ((col as u8 + b'a') as char, (row as u8 + b'1') as char)
}
/// Move is an unfinished structure representing a chess move.
#[non_exhaustive]
pub struct Move;

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
/// a request to
pub enum CastleRequest {
    BlackQueen,
    BlackKing,
    WhiteQueen,
    WhiteKing,
}
#[non_exhaustive]
/// todo: there are 16 possible en passants that could be valid, depending on the last move.
/// only one of them is possible.
#[derive(Copy, Clone, PartialEq, Eq, Debug, Serialize, Deserialize)]
pub enum Enpassant {
    None,
}
#[non_exhaustive]
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
/// an invalid move
pub enum Error {
    OutOfBounds,
    InvalidMove,
    Blocked,
    WouldBeInCheck,
    BadEnPassant,
    BadCastle,
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        // todo: implement this better
        eprintln!("this error message isn't good enough yet");
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for Error {}

#[allow(unreachable_code)]
#[allow(unused_variables)]
impl Gamestate {
    pub const fn new() -> Self {
        Gamestate {
            board: Board::DEFAULT,
            turn: 0,
            en_passant: Enpassant::None,
            castling: CastlingAllowed {
                black_king: true,
                black_queen: true,
                white_king: true,
                white_queen: true,
            },
        }
    }

    pub const fn active_player(&self) -> Color {
        if self.turn % 2 == 0 { White } else { Black }
    }

    pub fn can_castle(&self, r: CastleRequest) -> bool {
        todo!("hasn't moved or been captured") && todo!("spaces are cleared")
    }

    pub fn can_en_passant(&self, r: Enpassant) -> bool {
        todo!("enemy pawn did double move last turn")
            && todo!("friendly pawn is in appropriate position")
    }

    pub fn try_move(&self, src: (i8, i8), dst: (i8, i8)) -> Result<Self, Error> {
        let (src_piece, dst_piece): (Square, Square) = (self[src], self[dst]);
        if Board::out_of_bounds(src) || Board::out_of_bounds(dst) {
            Err(Error::OutOfBounds)
        } else if todo!("invalid move") {
            Err(Error::InvalidMove)
        } else if todo!("blocked") {
            Err(Error::Blocked)
        } else if let Some(enpassant_req) = todo!("is en passant") {
            if self.can_en_passant(enpassant_req) {
                todo!("en passant")
            } else {
                Err(Error::BadEnPassant)
            }
        } else if let Some(castling_request) = todo!("is castling request") {
            if self.can_castle(castling_request) {
                todo!("castle")
            } else {
                Err(Error::BadCastle)
            }
        } else {
            let board: Board = todo!("execute move");
            if board.player_in_check(self.active_player(), todo!("en passant")) {
                return Err(Error::WouldBeInCheck);
            } else {
                Ok(Self {
                    turn: self.turn + 1,
                    castling: CastlingAllowed {
                        white_king: self.castling.white_king
                            && todo!(
                                "source or dst squares not white king or king's rook's starter square"
                            ),
                        black_king: self.castling.black_king
                            && todo!(
                                "source or dst squares not white king or king's rook's starter square"
                            ),
                        white_queen: self.castling.white_queen
                            && todo!(
                                "source or dst squares not white king or queen's rook's starter square"
                            ),
                        black_queen: self.castling.white_queen
                            && todo!(
                                "source or dst squares not white king or queen's rook's starter square"
                            ),
                    },
                    en_passant: if todo!("was_double_pawn_move") {
                        todo!("some en passants")
                    } else {
                        Enpassant::None
                    },
                    board,
                })
            }
        }
    }
}

impl std::ops::Index<(i8, i8)> for Gamestate {
    type Output = Square;

    fn index(&self, i: (i8, i8)) -> &Self::Output {
        &self.board[i]
    }
}

impl std::ops::IndexMut<(i8, i8)> for Gamestate {
    fn index_mut(&mut self, i: (i8, i8)) -> &mut Self::Output {
        &mut self.board[i]
    }
}

impl std::ops::Index<(i8, i8)> for Board {
    type Output = Square;

    fn index(&self, (row, col): (i8, i8)) -> &Self::Output {
        assert!(row < 8 && row >= 0 && col < 8 && col >= 0);
        let (row, col) = (row as usize, col as usize);
        unsafe { self.0.get_unchecked(row).get_unchecked(col) }
    }
}

impl std::ops::IndexMut<(i8, i8)> for Board {
    fn index_mut(&mut self, (row, col): (i8, i8)) -> &mut Self::Output {
        assert!(row < 8 && row >= 0 && col < 8 && col >= 0);
        let (row, col) = (row as usize, col as usize);
        unsafe { self.0.get_unchecked_mut(row).get_unchecked_mut(col) }
    }
}
#[derive(Copy, Clone, PartialEq, Eq, Debug, Serialize, Deserialize)]
/// whether or not the specific castling is still valid
/// in other words, whether the king or rook has ever moved, even if they moved back to their original position.
struct CastlingAllowed {
    pub white_king: bool,
    pub white_queen: bool,
    pub black_king: bool,
    pub black_queen: bool,
}
#[derive(Copy, Clone, PartialEq, Eq, Debug, Serialize, Deserialize)]
/// The seven piece types in traditional chess.
pub enum Kind {
    Pawn,
    Rook,
    Bishop,
    King,
    Queen,
    Knight,
}
#[derive(Copy, Clone, PartialEq, Eq, Debug, Serialize, Deserialize)]
/// The two colors of chess. White goes first.
pub enum Color {
    White,
    Black,
}
#[derive(Copy, Clone, PartialEq, Eq, Debug, Serialize, Deserialize)]
/// A chess piece, defined uniquely by it's kind and color.
pub struct Piece(pub Kind, pub Color);

impl Default for Board {
    fn default() -> Self {
        Self::DEFAULT
    }
}
impl Board {
    /// the black king's home row:
    /// ```text
    /// ♜ ♞ ♝ ♛ ♚ ♝ ♞ ♜
    /// ```
    pub const BLACK_KINGS_ROW: [Square; 8] = [
        Some(Piece(Rook, Black)),
        Some(Piece(Knight, Black)),
        Some(Piece(Bishop, Black)),
        Some(Piece(Queen, Black)),
        Some(Piece(King, Black)),
        Some(Piece(Bishop, Black)),
        Some(Piece(Knight, Black)),
        Some(Piece(Rook, Black)),
    ];
    /// ```text
    /// ♜ ♞ ♝ ♛ ♚ ♝ ♞ ♜
    /// ♟ ♟ ♟ ♟ ♟ ♟ ♟ ♟
    ///
    ///
    ///
    ///
    /// ♙ ♙ ♙ ♙ ♙ ♙ ♙ ♙
    /// ♖ ♘ ♗ ♕ ♔ ♗ ♘ ♖
    /// ```
    pub const DEFAULT: Self = Self([
        Self::BLACK_KINGS_ROW,
        [Some(Piece(Pawn, Black)); 8],
        [None; 8],
        [None; 8],
        [None; 8],
        [None; 8],
        [Some(Piece(Pawn, White)); 8],
        Self::WHITE_KINGS_ROW,
    ]);
    pub const EMPTY: Self = Self([[None; 8]; 8]);
    /// the white king's home row:
    /// ```text
    /// ♖ ♘ ♗ ♕ ♔ ♗ ♘ ♖
    /// ```
    pub const WHITE_KINGS_ROW: [Square; 8] = [
        Some(Piece(Rook, White)),
        Some(Piece(Knight, White)),
        Some(Piece(Bishop, White)),
        Some(Piece(Queen, White)),
        Some(Piece(King, White)),
        Some(Piece(Bishop, White)),
        Some(Piece(Knight, White)),
        Some(Piece(Rook, White)),
    ];

    /// whether a square is out of bounds of the 8x8 chessboard.
    pub fn out_of_bounds((i, j): (i8, i8)) -> bool {
        return i < 0 || j < 0 || i > 8 || j > 8;
    }

    /// whether a square is in the bounds of the 8x8 chessboard
    pub fn in_bounds((i, j): (i8, i8)) -> bool {
        return !Board::out_of_bounds((i, j));
    }

    /// an iterator over all combinations of positions; i.e, the cartesian product of (0..8) with itself.
    pub fn positions() -> impl Iterator<Item = (i8, i8)> {
        (0..64_i8).map(|i| (i / 8, i % 8))
    }

    /// the squares in the given row, zero-indexed
    pub fn row(&self, i: usize) -> [Square; 8] {
        self.0[i]
    }

    /// valid moves for a rook starting from this square, assuming unblocked spaces.
    pub fn rooks_moves((row, col): (i8, i8)) -> impl Iterator<Item = (i8, i8)> {
        (0..8_i8)
            .map(move |i| (i, col))
            .chain((0..8).map(move |j| (row, j)))
    }

    /// valid moves for a bishop starting from this square, assuming unblocked spaces.
    pub fn bishops_moves((row, col): (i8, i8)) -> impl Iterator<Item = (i8, i8)> {
        let pos_pos = (0..8).map(move |i| (row + i, col + i));
        let pos_neg = (0..8).map(move |i| (row + i, col - i));
        let neg_pos = (0..8).map(move |i| (row - i, col + i));
        let neg_neg = (0..8).map(move |i| (row - i, col - i));
        pos_pos
            .chain(pos_neg)
            .chain(neg_pos)
            .chain(neg_neg)
            .filter(|s| Board::in_bounds(*s))
    }

    /// valid moves for a knight starting from this square
    pub fn knights_moves((row, col): (i8, i8)) -> impl Iterator<Item = (i8, i8)> {
        const KNIGHTS_MOVES: [(i8, i8); 8] = [
            (1, 2),
            (1, -2),
            (2, 1),
            (2, -1),
            (-1, 2),
            (-1, -2),
            (-2, 1),
            (-2, -1),
        ];
        KNIGHTS_MOVES
            .iter()
            .map(move |(i, j)| (row + i, col + j))
            .filter(|s| Board::in_bounds(*s))
    }

    /// the squares in the given column, zero-indexed
    pub fn col(&self, i: usize) -> [Square; 8] {
        let mut col = [None; 8];
        for (s, row) in col.iter_mut().zip(&self.0) {
            *s = row[i]
        }
        col
    }

    fn threatened_by_bishop_or_queen(&self, _: (i8, i8), _: Color) -> bool {
        todo!()
    }

    fn threatened_by_rook_or_queen(&self, (row, col): (i8, i8), color: Color) -> bool {
        for i in (0..row).rev() {
            match self[(i, col)] {
                Some(Piece(Rook, c)) | Some(Piece(Queen, c)) if c != color => return true,
                Some(Piece(..)) => break,
                None => continue,
            }
        }
        for i in (row + 1)..8 {
            match self[(i, col)] {
                Some(Piece(Rook, c)) | Some(Piece(Queen, c)) if c != color => return true,
                Some(Piece(..)) => break,
                None => continue,
            }
        }

        for j in (0..col).rev() {
            match self[(row, j)] {
                Some(Piece(Rook, c)) | Some(Piece(Queen, c)) if c != color => return true,
                Some(Piece(..)) => break,
                None => continue,
            }
        }
        for j in (col + 1)..8 {
            match self[(row, j)] {
                Some(Piece(Rook, c)) | Some(Piece(Queen, c)) if c != color => return true,
                Some(Piece(..)) => break,
                None => continue,
            }
        }

        false
    }

    pub fn threatened_by_king(&self, (row, col): (i8, i8), color: Color) -> bool {
        const KINGS_MOVES: [(i8, i8); 8] = [
            (1, 1),
            (1, 0),
            (1, -1),
            (0, 1),
            (0, -1),
            (-1, 1),
            (-1, 0),
            (-1, -1),
        ];
        KINGS_MOVES
            .iter()
            .map(move |(i, j)| (row + i, col + j))
            .filter(|s| Board::in_bounds(*s))
            .any(|pos| match self[pos] {
                Some(Piece(King, c)) if c != color => true,
                _ => false,
            })
    }

    fn threatened_by_knight(&self, (row, col): (i8, i8), color: Color) -> bool {
        Board::knights_moves((row, col)).any(|pos| match self[pos] {
            Some(Piece(Rook, c)) if c != color => true,
            _ => false,
        })
    }

    fn threatened_by_pawn(&self, _: (i8, i8), _: Color, _: Enpassant) -> bool {
        todo!("normal capture");
        todo!("en passant")
    }

    fn square_is_threatened(&self, pos: (i8, i8), color: Color, enpassant: Enpassant) -> bool {
        self.threatened_by_rook_or_queen(pos, color)
            || self.threatened_by_bishop_or_queen(pos, color)
            || self.threatened_by_knight(pos, color)
            || self.threatened_by_king(pos, color)
            || self.threatened_by_pawn(pos, color, enpassant)
    }

    /// return true if the player is in check. this will panic if there's no king on the board.
    /// if there are somehow multiple kings, this prioritizes the king with the lowest index.
    fn player_in_check(&self, color: Color, enpassant: Enpassant) -> bool {
        // ideas on how to do this efficently:
        // find the king, then check the knight's moves away from him and the cardinal directions until they're blocked or hit an opponent that could hit him.

        let kings_pos = Board::positions()
            .find(|pos| match self[*pos] {
                Some(Piece(King, c)) if color == c => true,
                _ => false,
            })
            .expect("expected to find a king, but didn't");
        self.square_is_threatened(kings_pos, color, enpassant)
    }
}
