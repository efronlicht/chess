use super::*;

fn parse_pgn(_s: &str) -> Vec<Move> {
    todo!("find a library that parses portable game notation, or do it myself")
}
use std::fmt::Display;
#[test]
#[ignore="need pgn parsing and game logic"]
fn full_games() {  
    struct Test {
        game: &'static str,
        want_boardstate: [[Option<Piece>; 8]; 8],
    }
    for Test{game, want_boardstate} in &[Test{game: include_str!("games/kasparov_vs_topalov.pgn"), want_boardstate: [
        [None; 8],
        [Some(Piece(Queen, White)), None, None, None, None, None, None, Some(Piece(Pawn, Black))],
        [None, None, None, None, None, None, Some(Piece(Pawn, Black)),  None],
        [None, None, None, None, None,  Some(Piece(Pawn, Black)), None, None],
        [None, None, None, None, None, Some(Piece(Pawn, White)), None,  None],
        [None, None, Some(Piece(Pawn, Black)), None,None, None,  Some(Piece(Pawn, White)),   None],
        [None, None, None, Some(Piece(Rook, Black)), None, None, None, Some(Piece(Pawn, White))],
        [None, None, Some(Piece(King, White)), None, Some(Piece(King, Black)), None, None, None]
    ]},
    Test{game: include_str!("games/aronian_vs_anand.pgn"), want_boardstate: todo!("write out final gamestate")}] {

        let got = parse_pgn(&game).into_iter().try_fold(Gamestate::new(), Gamestate::try_move).unwrap();
        assert_eq!(got.board.to_string().trim(), Board(*want_boardstate).to_string())
    }


}