//! human-friendly text encodings for game pieces using the unicode chess symbols. this _could_ be used as a reasonably efficient serialization library, but that's not it's purpose.
use crate::{
    Board,
    Color::{Black, White},
    Kind::*,
    Piece,
};
use std::{convert::TryFrom, fmt};

pub const WHITE_KING: char = '♔';
pub const WHITE_QUEEN: char = '♕';
pub const WHITE_ROOK: char = '♖';
pub const WHITE_BISHOP: char = '♗';
pub const WHITE_KNIGHT: char = '♘';
pub const WHITE_PAWN: char = '♙';

pub const BLACK_KING: char = '♚';
pub const BLACK_QUEEN: char = '♛';
pub const BLACK_ROOK: char = '♜';
pub const BLACK_BISHOP: char = '♝';
pub const BLACK_KNIGHT: char = '♞';
pub const BLACK_PAWN: char = '♟';
/// the "mutton" em-space.
pub const EM_SPACE: char = '\u{2003}';

#[derive(Copy, Clone, Debug)]
pub enum DeserializationError {
    TooManyRows,
    TooFewRows,
    RowTooLong,
    RowTooShort,
    UnknownChar(char),
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        const BYTES_PIECES: usize = 8 * 8 * 4;
        const BYTES_EMSPACES: usize = 8 * 7 * 4;
        const BYTES_NEWLINES: usize = 8;

        let mut symbols = String::with_capacity(BYTES_EMSPACES + BYTES_PIECES + BYTES_NEWLINES + 2);
        for row in 0..8 {
            for col in 0..8 {
                let c = if let Some(p) = self[(row, col)] {
                    p.into()
                } else {
                    EM_SPACE
                };
                symbols.push(c);
                symbols.push(EM_SPACE);
            }
            symbols.pop(); // remove trailing em-space
            symbols.push('\n');
        }
        f.write_str(&symbols)
    }
}

impl Into<char> for Piece {
    fn into(self) -> char {
        match self {
            Piece(King, White) => WHITE_KING,
            Piece(Queen, White) => WHITE_QUEEN,
            Piece(Rook, White) => WHITE_ROOK,
            Piece(Bishop, White) => WHITE_BISHOP,
            Piece(Knight, White) => WHITE_KNIGHT,
            Piece(Pawn, White) => WHITE_PAWN,

            Piece(King, Black) => BLACK_KING,
            Piece(Queen, Black) => BLACK_QUEEN,
            Piece(Rook, Black) => BLACK_ROOK,
            Piece(Bishop, Black) => BLACK_BISHOP,
            Piece(Knight, Black) => BLACK_KNIGHT,
            Piece(Pawn, Black) => BLACK_PAWN,
        }
    }
}
impl<'a> Into<char> for &'a Piece {
    fn into(self) -> char {
        (*self).into()
    }
}

impl std::fmt::Display for DeserializationError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::TooManyRows => write!(f, "too many rows: expected 8 rows"),
            Self::TooFewRows => write!(f, "too few rows: expected 8 rows"),
            Self::RowTooLong => write!(f, "row too long: expected 8 pieces"),
            Self::RowTooShort => write!(f, "row too short: expected 8 pieces"),
            Self::UnknownChar(c) => write!(
                f,
                "unknown chesspiece: expected a chesspiece, but got {}: \\u{}",
                c, *c as u32
            ),
        }
    }
}
impl std::str::FromStr for Board {
    type Err = DeserializationError;

    fn from_str(s: &str) -> Result<Self, DeserializationError> {
        let mut out = Self([[None; 8]; 8]);
        let mut row = 0;
        for line in s.lines() {
            if row >= 8 {
                return Err(DeserializationError::TooManyRows);
            }
            let mut col = 0;
            for c in line.chars().filter(|c| !c.is_whitespace()) {
                if col >= 8 {
                    return Err(DeserializationError::RowTooLong);
                }
                out.0[row][col] = match Piece::try_from(c) {
                    Ok(p) => Some(p),
                    Err(_) => return Err(DeserializationError::UnknownChar(c)),
                };
                col += 1;
            }
            if col < 8 {
                return Err(DeserializationError::RowTooShort);
            }
            row += 1;
        }

        if row < 8 {
            return Err(DeserializationError::TooFewRows);
        }
        Ok(out)
    }
}
impl TryFrom<char> for Piece {
    type Error = &'static str;

    fn try_from(c: char) -> Result<Self, Self::Error> {
        Ok(match c {
            WHITE_KING => Piece(King, White),
            WHITE_QUEEN => Piece(Queen, White),
            WHITE_ROOK => Piece(Rook, White),
            WHITE_BISHOP => Piece(Bishop, White),
            WHITE_KNIGHT => Piece(Knight, White),
            WHITE_PAWN => Piece(Pawn, White),
            BLACK_KING => Piece(King, Black),
            BLACK_QUEEN => Piece(Queen, Black),
            BLACK_ROOK => Piece(Rook, Black),
            BLACK_BISHOP => Piece(Bishop, Black),
            BLACK_KNIGHT => Piece(Knight, Black),
            BLACK_PAWN => Piece(Pawn, Black),
            _ => {
                return Err(
                    "unknown chess piece symbol. expected one of the chess unicode symbols from U+2654..=U+265F (♔..=♟︎",
                );
            }
        })
    }
}

impl fmt::Display for Piece {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let c: char = self.into();
        write!(f, "{}", c)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_display() {
        const WANT: &str = r#"
♜ ♞ ♝ ♛ ♚ ♝ ♞ ♜
♟ ♟ ♟ ♟ ♟ ♟ ♟ ♟
               
               
               
               
♙ ♙ ♙ ♙ ♙ ♙ ♙ ♙
♖ ♘ ♗ ♕ ♔ ♗ ♘ ♖
"#;
        assert_eq!(WANT.trim(), format!("{}", super::Board::DEFAULT).trim());
    }
}
