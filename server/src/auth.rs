//! authentication, cryptography, and credentials
use rand::{rngs::OsRng, Rng};
use rocket::request;
use sha2::Digest;
use std::collections::HashMap;
/// 128 randomly-generated bits via the OS.
pub type Salt = [u8; 128];
/// the output from SHA256.
pub type Hash = [u8; 32];
#[derive(Clone, PartialEq, Eq, Debug)]
// An error logging in via http basicauth
pub enum LoginError {
    UnknownUser,
    BadCredentials,
    BadUtf8(std::str::Utf8Error),
    BadBase64(base64::DecodeError),
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub enum UserLevel {
    Regular,
    Admin,
}

struct UserInfo {
    id: u128,
    username: String,
    salted_password_hash: Hash,
    salt: Salt,
    level: UserLevel,
}
const BASIC_AUTH_HEADER_KEY: &str = "Authorization";

pub fn get_basicauth(
    headers: &rocket::http::HeaderMap,
) -> Result<Option<(String, String)>, LoginError> {
    let header = if let Some(header) = headers.get_one(BASIC_AUTH_HEADER_KEY) {
        header
    } else {
        return Ok(None);
    };
    let base64_auth = if let Some(("Basic", authstr)) = split_once(header, ' ') {
        authstr
    } else {
        return Err(LoginError::BadCredentials);
    };

    let decoded_auth =
        String::from_utf8(base64::decode(base64_auth)?).map_err(|err| err.utf8_error())?;
    if let Some((username, password)) = split_once(&decoded_auth, ':') {
        Ok(Some((username.to_string(), password.to_string())))
    } else {
        Err(LoginError::BadCredentials)
    }
}
#[non_exhaustive]
pub struct Users {
    from_id: HashMap<u128, UserInfo>,
    id_from_username: HashMap<String, u128>,
}

fn salt_and_hash(password: &str) -> (Salt, Hash) {
    let mut salt: Salt = [0; 128];
    OsRng {}.fill(&mut salt);
    (salt, hash(password, &salt))
}

fn hash(password: &str, salt: &Salt) -> [u8; 32] {
    let mut hasher = sha2::Sha256::new();
    hasher.update(password);
    hasher.update(salt);
    let mut output: Hash = [0; 32];
    output.copy_from_slice(hasher.finalize().as_slice());
    output
}

impl Users {
    pub fn test_users() -> Users {
        let mut users = Users {
            from_id: HashMap::new(),
            id_from_username: HashMap::new(),
        };
        users.add_user_with_id(1, "efron", "licht", UserLevel::Admin);
        users.add_user_with_id(2, "magnus", "carlsen", UserLevel::Regular);
        users
    }
}

use super::{Player, User};
impl Users {
    pub fn add_user(
        &mut self,
        username: &str,
        password: &str,
        level: UserLevel,
    ) -> Result<(), &'static str> {
        if self.id_from_username.contains_key(username) {
            return Err("username already taken");
        } else if password.len() <= 4 {
            return Err("password too short: need at least 4 characters");
        }
        let (salt, salted_password_hash) = salt_and_hash(password);
        let id = OsRng {}.gen();
        let user = UserInfo {
            username: username.to_string(),
            salted_password_hash,
            salt,
            level,
            id, // odds of collision are essentially zero; this is the same practice the v4 uuid uses.
        };
        self.id_from_username.insert(username.to_string(), id);
        self.from_id.insert(id, user);
        Ok(())
    }

    /// this function is just for tests and will panic in release mode. so be careful!
    fn add_user_with_id(
        &mut self,
        id: u128,
        username: &str,
        password: &str,
        level: UserLevel,
    ) -> UserInfo {
        assert!(
            cfg!(debug_assertions),
            "this function should never be called in release mode"
        );
        if self.id_from_username.contains_key(username) {
            panic!("username taken")
        } else if password.len() <= 4 {
            panic!("password too short: need at least 4 characters")
        }
        let (salt, salted_password_hash) = salt_and_hash(password);
        UserInfo {
            username: username.to_string(),
            salted_password_hash,
            salt,
            level,
            id,
        }
    }

    pub fn auth(&self, username: &str, password: &str) -> Result<User, LoginError> {
        let id = match self.id_from_username.get(username) {
            None => return Err(LoginError::UnknownUser),
            Some(id) => *id,
        };
        let userinfo = &self.from_id[&id];
        if userinfo.salted_password_hash != hash(password, &userinfo.salt) {
            return Err(LoginError::BadCredentials);
        }
        match userinfo.level {
            UserLevel::Admin => Ok(User::Admin(Player {
                id,
                handle: username.to_string(),
            })),
            UserLevel::Regular => Ok(User::LoggedIn(Player {
                id,
                handle: username.to_string(),
            })),
        }
    }
}

impl std::fmt::Display for LoginError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            LoginError::UnknownUser => write!(f, "unknown username"),
            LoginError::BadCredentials => write!(
                f,
                "bad credentials: your password may be wrong or you may have an improperly formatted http basicauth header"
            ),
            LoginError::BadUtf8(err) => write!(
                f,
                "the provided username or password was not valid utf8: {}",
                err
            ),
            LoginError::BadBase64(err) => write!(
                f,
                "the authorization section of your http basicauth header was not valid base64: {}",
                err
            ),
        }
    }
}
impl std::error::Error for LoginError {}

impl<T> Into<request::Outcome<T, LoginError>> for LoginError {
    fn into(self) -> request::Outcome<T, LoginError> {
        let status = match self {
            LoginError::UnknownUser | LoginError::BadUtf8(_) | LoginError::BadBase64(_) => {
                rocket::http::Status::Unauthorized
            }
            LoginError::BadCredentials => rocket::http::Status::Forbidden,
        };
        rocket::request::Outcome::Failure((status, self))
    }
}

impl From<base64::DecodeError> for LoginError {
    fn from(err: base64::DecodeError) -> Self {
        Self::BadBase64(err)
    }
}

impl From<std::str::Utf8Error> for LoginError {
    fn from(err: std::str::Utf8Error) -> Self {
        Self::BadUtf8(err)
    }
}
fn split_once(s: &str, c: char) -> Option<(&str, &str)> {
    let mut split = s.split(c);
    split.next().zip(split.next())
}

#[cfg(test)]
mod tests {
    use super::*;
    use rocket::http::{Header, HeaderMap};
    #[test]
    fn test_basicauth_happypath() {
        let username = "username";
        let password = "password";
        let joined = format!("{}:{}", username, password);
        let base64_joined = base64::encode(joined);
        let auth = format!("Basic {}", base64_joined);
        let mut headers = HeaderMap::new();
        headers.add(Header::new(BASIC_AUTH_HEADER_KEY, auth));
        assert_eq!(
            Ok(Some((username.to_string(), password.to_string()))),
            get_basicauth(&headers)
        );
    }
    #[test]
    fn test_basicauth_no_header() {
        assert_eq!(Ok(None), get_basicauth(&HeaderMap::new()));
    }
    #[test]
    fn test_basicauth_errors() {
        for (label, value) in vec![
            (
                "no colon",
                format!("Basic {}", base64::encode("username_password")),
            ), // no colon,
            ("not base64", format!("Basic {}", "username:password")),
            (
                "doesn't start with Basic",
                base64::encode("username:password"),
            ),
        ] {
            let mut headers = HeaderMap::new();
            headers.add(Header::new(BASIC_AUTH_HEADER_KEY, value));
            assert!(get_basicauth(&headers).is_err(), "{}", label);
        }
    }
}
