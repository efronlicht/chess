//! a chess server which handles users, storage, etc. the actual game logic is in [game].
#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
pub mod auth;
pub mod routes;
use rocket::{request, Outcome, Request, State};
use serde::{Deserialize, Serialize};
use std::sync::RwLock;
type UTC = chrono::DateTime<chrono::Utc>;

/// build the server, but don't launch it.
pub fn build() -> rocket::Rocket {
    rocket::ignite()
        .attach(rocket::fairing::AdHoc::on_request(
            "print request",
            print_req,
        ))
        .attach(rocket::fairing::AdHoc::on_response(
            "print response",
            print_resp,
        ))
        .manage(RwLock::new(auth::Users::test_users()))
        .manage(RwLock::new(test_games()))
        .mount(
            "/",
            routes![
                routes::index,
                routes::get_my_games,
                routes::game_by_id,
                routes::game_info_by_id
            ],
        )
}
#[cfg(debug_assertions)]
fn print_req(req: &mut rocket::Request, _: &rocket::Data) {
    eprintln!("received request:\n{}", req);
}
#[cfg(not(debug_assertions))]
fn print_req(_: &mut rocket::Request, _: &rocket::Data) {}

#[cfg(debug_assertions)]
fn print_resp(_: &rocket::request::Request, resp: &mut rocket::response::Response) {
    let mut tw = tabwriter::TabWriter::new(std::io::stderr());

    writeln!(tw, "wrote response").unwrap();
    writeln!(tw, "Status\t{}", resp.status()).unwrap();
    let headers = resp.headers();
    use std::io::Write;
    if !headers.is_empty() {
        writeln!(tw, "headers:\n").unwrap();
        for header in headers.iter() {
            writeln!(tw, "{}\t{}", header.name(), header.value()).unwrap();
        }
        writeln!(tw).unwrap();
        tw.flush().unwrap();

    }
    if let Some(body) = resp.body_string() {
        eprintln!("body:\n{}", body);
        resp.set_sized_body(std::io::Cursor::new(body));
    }

}
#[cfg(not(debug_assertions))]
fn print_resp(_: &rocket::request::Request, _: &mut rocket::response::Response) {}

#[derive(Copy, Clone, PartialEq, Eq, Debug, Serialize, Deserialize)]
pub enum Privacy {
    Public,
    Private,
    LoggedIn,
}
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Player {
    id: u128,
    handle: String,
}

impl std::fmt::Display for Player {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.handle)
    }
}
impl PartialEq for Player {
    fn eq(&self, other: &Self) -> bool {
        return self.id == other.id;
    }
}
impl Eq for Player {}

#[derive(Clone, PartialEq, Eq, Debug, Serialize, Deserialize)]
pub enum User {
    /// An administrator who can view any game.
    Admin(Player),
    /// Someone with an account who isn't an administrator.
    LoggedIn(Player),
    /// Someone who's not logged in at all.
    Guest,
}

pub fn test_games() -> std::collections::HashMap<u128, GameInfo> {
    let mut games = std::collections::HashMap::new();
    let start_time_utc = UTC::from_utc(chrono::NaiveDateTime::from_timestamp(100, 0), chrono::Utc);
    games.insert(
        01,
        GameInfo {
            game_id: 01,
            privacy: Privacy::Public,
            black: Player {
                id: 00,
                handle: "efron".to_string(),
            },
            white: Player {
                id: 01,
                handle: "magnus".to_string(),
            },
            gamestate: game::Gamestate::new(),
            start_time_utc,
            last_move_at_utc: start_time_utc.clone(),
        },
    );

    games.insert(
        02,
        GameInfo {
            game_id: 02,
            privacy: Privacy::Private,
            black: Player {
                id: 00,
                handle: "efron".to_string(),
            },
            white: Player {
                id: 01,
                handle: "magnus".to_string(),
            },
            gamestate: game::Gamestate::new(),
            start_time_utc,
            last_move_at_utc: start_time_utc.clone(),
        },
    );
    games
}

impl<'a, 'r> rocket::request::FromRequest<'a, 'r> for User {
    type Error = auth::LoginError;

    fn from_request(req: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let users = req
            .guard::<State<std::sync::RwLock<auth::Users>>>()
            .unwrap();
        match auth::get_basicauth(req.headers()) {
            Ok(Some((username, password))) => {
                match users.read().unwrap().auth(&username, &password) {
                    Err(err) => err.into(),
                    Ok(user) => Outcome::Success(user),
                }
            }
            Ok(None) => Outcome::Success(User::Guest),
            Err(err) => err.into(),
        }
    }
}

#[derive(Clone, PartialEq, Eq, Debug, Serialize, Deserialize)]
pub struct GameInfo {
    game_id: u128,
    gamestate: game::Gamestate,
    privacy: Privacy,
    black: Player,
    white: Player,
    start_time_utc: UTC,
    last_move_at_utc: UTC,
}
