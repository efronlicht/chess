//! the actual http handlers

use super::{GameInfo, Player, Privacy, User};
use rocket::State;
use rocket_contrib::json::Json;
use std::{collections::HashMap, sync::RwLock};
type UTC = chrono::DateTime<chrono::Utc>;
use crate::auth;
use rocket::http::Status;
#[get("/")]
pub fn index() -> String {
    // these should be kept sorted.
    const ROUTE_DESCRIPTIONS: &[(&str, &str)] = &[
        ("/", "list of routes (this page)"),
        ("/game/<id>", "view the game with the given id"),
        (
            "/game/<id>/info",
            "get extended information about the game with the given id",
        ),
        ("/player/games", "get a list of games you're in"),
    ];
    use std::io::Write;
    let mut tw = tabwriter::TabWriter::new(Vec::with_capacity(150));
    for (route, desc) in ROUTE_DESCRIPTIONS {
        writeln!(tw, "{}\t{}", route, desc).unwrap();
    }
    String::from_utf8(tw.into_inner().unwrap()).unwrap()
}
#[get("/player/games")]
pub fn get_my_games(
    user: User,
    games: State<RwLock<HashMap<u128, GameInfo>>>,
) -> Result<Json<Vec<(UTC, u128)>>, rocket::http::Status> {
    match user {
        User::Guest => Err(rocket::http::Status::Unauthorized),
        User::Admin(p) | User::LoggedIn(p) => {
            let mut players_games: Vec<(UTC, u128)> = games
                .read()
                .unwrap()
                .values()
                .filter(|gameinfo| gameinfo.black == p || gameinfo.white == p)
                .map(|gameinfo| (gameinfo.last_move_at_utc, gameinfo.game_id))
                .collect();
            players_games.sort();
            players_games.reverse();
            Ok(Json(players_games))
        }
    }
}

#[derive(Clone, Debug, serde::Deserialize)]
struct NewUserRequest {
    username: String,
    password: String,
}

#[derive(Clone, Debug, serde::Deserialize)]
struct NewAdminRequest {
    username: String,
    password: String,
    token: String,
}

#[post("/player/new", data = "<req>")]
/// todo: some sort of more sophisticated validation
fn new_player(req: Json<NewUserRequest>, users: State<RwLock<auth::Users>>) -> Result<(), Status> {
    if req.username.len() < 4 || req.password.len() < 4 {
        Err(Status::BadRequest)
    } else {
        users
            .write()
            .unwrap()
            .add_user(&req.username, &req.password, auth::UserLevel::Regular)
            .map_err(|reason| Status { code: 400, reason })
    }
}

#[post("/admin/new", data = "<req>")]
fn new_admin(req: Json<NewAdminRequest>, users: State<RwLock<auth::Users>>) -> Result<(), Status> {
    // todo: some kind of credential management
    const ADMIN_TOKEN: &str = "AN_EXTREMELY_GOOD_IDEA";

    if req.token != ADMIN_TOKEN {
        Err(Status::Forbidden)
    } else if req.username.len() < 4 || req.password.len() < 4 {
        Err(Status::BadRequest)
    } else {
        users
            .write()
            .unwrap()
            .add_user(&req.username, &req.password, auth::UserLevel::Admin)
            .map_err(|reason| Status { code: 400, reason })
    }
}

#[get("/game/<game_id>/info")]
pub fn game_info_by_id(
    game_id: u128,
    user: User,
    games: State<RwLock<HashMap<u128, GameInfo>>>,
) -> Result<Json<GameInfo>, Status> {
    let game = match games.read().unwrap().get(&game_id) {
        Some(game) => game.clone(),
        None => return Err(Status::NotFound),
    };
    match (game.privacy, user) {
        (Privacy::Public, _) | (_, User::Admin(..)) | (Privacy::LoggedIn, User::LoggedIn(_)) => {
            Ok(Json(game))
        }
        (Privacy::Private, User::LoggedIn(Player { id, .. }))
            if game.black.id == id || game.white.id == id =>
        {
            Ok(Json(game))
        }
        (Privacy::Private, User::LoggedIn(_)) => Err(Status::Forbidden),
        (_, User::Guest) => Err(Status::Unauthorized),
    }
}

#[get("/game/<game_id>")]
pub fn game_by_id(
    game_id: u128,
    user: User,
    games: State<RwLock<HashMap<u128, GameInfo>>>,
) -> Result<String, Status> {
    let game = match games.read().unwrap().get(&game_id) {
        Some(game) => game.clone(),
        None => return Err(Status::NotFound),
    };
    match (game.privacy, user) {
        (Privacy::Public, _) | (_, User::Admin(..)) | (Privacy::LoggedIn, User::LoggedIn(_)) => {
            Ok(game.gamestate.board.to_string())
        }
        (Privacy::Private, User::LoggedIn(Player { id, .. }))
            if game.black.id == id || game.white.id == id =>
        {
            Ok(game.gamestate.board.to_string())
        }
        (Privacy::Private, User::LoggedIn(_)) => Err(Status::Forbidden),
        (_, User::Guest) => Err(Status::Unauthorized),
    }
}
